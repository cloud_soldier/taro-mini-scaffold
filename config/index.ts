const path = require("path");
const { UnifiedWebpackPluginV5 } = require('weapp-tailwindcss/webpack')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const config = {
  projectName: 'slt-mini',
  alias: {
    "@": path.resolve(__dirname, '..', 'src'),
  },
  date: '2023-11-7',
  designWidth (input) {
    // 配置 NutUI 375 尺寸
    if (input?.file?.replace(/\\+/g, '/').indexOf('@nutui') > -1) {
      return 375
    }
    // 全局使用 Taro 默认的 750 尺寸
    return 750
  },
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: 2 / 1
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  plugins: ['@tarojs/plugin-html'],
  sass: {
    // data: `@import "src/custom_theme.scss";@import "@nutui/nutui-react-taro/dist/styles/variables.scss";`,
  },
  defineConstants: {
  },
  copy: {
    patterns: [
      { from: 'src/assets/images/', to: 'dist/assets/images/', ignore: ['*.js'] },
    ],
    options: {
    }
  },
  framework: 'react',
  compiler: {
    type: 'webpack5',
    prebundle: {
      exclude: ['@nutui/nutui-react-taro', '@nutui/icons-react-taro']
    }
  },
  mini: {
    // 智能分包配置, 使用文档: https://docs.taro.zone/docs/next/mini-split-chunks-plugin/
    optimizeMainPackage:{
      enable: true,
    },
    // mini-css-extract-plugin插件配置
    miniCssExtractPluginOption: {
      ignoreOrder: true,  // 控制css的引入顺序不一致是否警告，false表示警告，true表示不警告
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {
          selectorBlackList: ['nut-'],
          // 用于解决nutui和tailwindcss的兼容性问题
          // 设置成 false 表示 不去除 * 相关的选择器区块
          // 假如开启这个配置，它会把 tailwindcss 整个 css var 的区域块直接去除掉
          config: {
            removeCursorStyle: false,
          },
        }
      },
      url: {
        enable: true,
        config: {
          limit: 1024 // 设定转换尺寸上限
        }
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: 'module', // 转换模式，取值为 global/module
          generateScopedName: '[name]__[local]___[hash:base64:5]'
        }
      }
    },

    webpackChain(chain, webpack) {
      chain.merge({
        plugin: {
          // 配置支持tailwindcss
          install: {
            plugin: UnifiedWebpackPluginV5,
            args: [{
              appType: 'taro'
            }]
          },
          // 配置支持typescript类型检查
          tsChecker:{
            plugin: ForkTsCheckerWebpackPlugin,
            args:[{}]
          }
        }
      })
    }
  },
  // h5: {
  //   publicPath: '/',
  //   staticDirectory: 'static',
  //   // esnextModules: ['nutui-react'],
  //   postcss: {
  //     pxtransform: {
  //       enable: true,
  //       config: {
  //         selectorBlackList: ['nut-']
  //       }
  //     },
  //     autoprefixer: {
  //       enable: true,
  //       config: {
  //       }
  //     },
  //     cssModules: {
  //       enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
  //       config: {
  //         namingPattern: 'module', // 转换模式，取值为 global/module
  //         generateScopedName: '[name]__[local]___[hash:base64:5]'
  //       }
  //     }
  //   }
  // }
}

module.exports = function (merge) {
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  }
  return merge({}, config, require('./prod'))
}
