import React from 'react';
import Taro from "@tarojs/taro";
import { WebView } from '@tarojs/components';

const Webview: React.FC = () => {
  const params = Taro.getCurrentInstance().router?.params;
  const { redirectUrl = "" } = params ?? {}
  // console.log("params->", params)
  return <WebView src={decodeURIComponent(redirectUrl)} />
}

export default Webview
