/**
 * 响应数据类型
 */
export declare namespace RespDataType {
  export interface ICaptchaImageInfo {
    img: string,
    // uuid: string
    key: string
  }

  export interface UserInfo {
    user: {
      id: number,
      userName: string,
      mobile: string,
      userNickname: string,
      birthday: number,
      userPassword: string,
      userSalt: string,
      userStatus: number,
      userEmail: string,
      sex: number,
      avatar: string,
      deptId: number,
      remark: string,
      isAdmin: number,
      address: string,
      describe: string,
      lastLoginIp: string,
      lastLoginTime: string,
      createdAt: string,
      updatedAt: string,
      deletedAt: string,
    },
    isSuperAdmin: boolean,
    permissions: string[],
  }
}

/**
 * 请求数据类型
 */
export declare namespace RequestDataType {
  export interface LoginParams {
    loginName: string;
    password: string;
    verifyCode?: string,
    verifyKey?: string,
  }
}
