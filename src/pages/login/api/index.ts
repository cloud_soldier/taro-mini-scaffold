import { request } from "@/utils/T";
import { RespDataType, RequestDataType } from "./apiType";

export * from "./apiType";
const { postJSON, get } = request;

/**
 * 获取验证码
 */
export const getCaptchaImage = () => get<RespDataType.ICaptchaImageInfo>("api/v1/pub/captcha/get");

/**
 * 登录
 */
export const login = (params: RequestDataType.LoginParams) => postJSON("api/v1/system/login", params);   // 新版


// /**
//  * 检查是否登录
//  */
// export const checkLogin = (isShowErrorPrompt = true) => new Promise((resolve, reject) => {
//   get(toApi("checkLogin"), {}, { isShowErrorPrompt }).then(() => {
//     resolve(true)
//   }).catch((e) => {
//     permission.loginUser.clear()
//     reject(e)
//   })
// });

/**
 * 获取用户信息
 */
export const getUserInfo = () => get<RespDataType.UserInfo>("api/v1/system/user/info").then((resp) => {
  // permission.loginUser.set(resp.data)
  console.log("resp->", resp.data);

}).catch((e) => {
  // permission.loginUser.clear();
  console.error(e);
});
