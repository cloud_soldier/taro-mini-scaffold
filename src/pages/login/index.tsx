import React from 'react'
import { View, Text } from '@tarojs/components';
import { Button, Form, Input, Image, Row, Col } from "@nutui/nutui-react-taro"

import { Scaffold } from '@/layout';
import { EnumSource, EnumEnv } from '@/constants';
import { useLogin } from './useLogin';

const Login: React.FC = () => {
  const { loading, handleSubmit, form, captchaImageInfo, fetchCaptchaImage } = useLogin();

  return <Scaffold
    style={{
      backgroundImage: `url("${EnumSource.local.loginBgImg}")`,
      backgroundSize: "100% 100%",
      paddingTop: "30%"
    }}
  >

    <View className='flex flex-col items-center justify-center'>
      <Image src={EnumSource.local.logoImg} width={128} height={128} />
      <Text className='mt-2' style={{ fontSize: 28 }}>{EnumEnv.app.title}</Text>
    </View>

    <View style={{ padding: "0 20px", marginTop: 50 }}>
      <Form
        form={form}
        onFinish={handleSubmit}
        // onFinishFailed={handleSubmit}
        footer={
          <>
            <Button loading={loading} onClick={form.submit} block type="primary">
              登录
            </Button>
          </>
        }
      >
        <Form.Item
          name="loginName"
          rules={[
            { required: true, message: '请输入用户名或手机号' },
          ]}
        >
          <Input
            placeholder="请输入用户名或手机号"
            type="text"
          />
        </Form.Item>

        <Form.Item
          // label="密码"
          name="password"
          rules={[
            { required: true, message: '请输入密码' },
          ]}
        >
          <Input
            placeholder="请输入密码"
            type="password"
          />
        </Form.Item>

        <Row align='center'>
          <Col span={18}>
            <Form.Item
              name="verifyCode"
              rules={[
                { required: true, message: '请输入验证码' },
              ]}
            >
              <Input
                placeholder="请输入验证码"
                type="text"
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Image src={captchaImageInfo.img} width={"100%"} height={40} onClick={fetchCaptchaImage} />
          </Col>
        </Row>

      </Form>
    </View>
  </Scaffold>
}

export default Login
