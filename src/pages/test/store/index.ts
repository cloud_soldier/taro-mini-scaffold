import { makeAutoObservable } from "mobx";

export class HomeStore{
  currentTime = Date.now();

  constructor(){
    makeAutoObservable(this,{})
  }

  setCurrentTime = (time: number) => {
    this.currentTime = time;
  }
}
