import React from 'react';
import Taro from '@tarojs/taro';
import { Button, Space } from "@nutui/nutui-react-taro";

import { EnumRouter, getWebviewPagePath } from '@/constants';

export const Navigation: React.FC = () => {

  return <Space wrap>
    <Button type="primary" onClick={() => {
      // 跳转到目的页面，打开新页面
      Taro.redirectTo({
        url: EnumRouter.login.path,
      })
    }}>
      redirectTo登录页面
    </Button>
    <Button type="primary" onClick={() => {
      // 跳转到目的页面，打开新页面
      Taro.navigateTo({
        url: EnumRouter.login.path,
      })
    }}>
      navigateTo登录页面
    </Button>
    <Button type="primary" onClick={() => {
      // 跳转到目的页面，打开新页面
      Taro.navigateTo({
        url: getWebviewPagePath("https://www.baidu.com?param1=1", {param2: 2}),
      })

    }}>
      跳转到webview页面
    </Button>
    <Button type="primary" onClick={() => {
      // 跳转到目的页面，打开新页面
      Taro.navigateTo({
        url: EnumRouter.subpackages.packageA.test.path,
      })
    }}>
      跳转到分包A的test页面
    </Button>
    <Button type="primary" onClick={() => {
      // 跳转到目的页面，打开新页面
      Taro.navigateTo({
        url: EnumRouter.subpackages.packageB.test.path,
      })
    }}>
      跳转到分包B的test页面
    </Button>
  </Space>
}


