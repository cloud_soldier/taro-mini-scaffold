import React from 'react';
import { Button, Space } from "@nutui/nutui-react-taro";

import prompt from '@/utils/prompt';

export const Prompt: React.FC = () => {

  return <Space wrap>
    <Button type="primary" onClick={() => prompt.info("提示消息")}>提示消息</Button>
    <Button type="primary" onClick={() => prompt.success("提示成功")}>提示成功</Button>
    <Button type="primary" onClick={() => prompt.warn("提示警告")}>提示警告</Button>
    <Button type="primary" onClick={() => prompt.error("提示失败")}>提示失败</Button>
    <Button type="primary" onClick={() => prompt.openDialog({
      title: '打开对话框',
      content: '可通过 prompt.openDialog 打开对话框',
      onConfirm: () => {
        prompt.closeDialog()
      },
      onCancel: () => {
        prompt.closeDialog()
      },
    })}>打开对话框</Button>
  </Space>
}


