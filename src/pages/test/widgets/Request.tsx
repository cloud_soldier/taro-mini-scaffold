import React from 'react';
import Taro from '@tarojs/taro';
import { Button, Space } from "@nutui/nutui-react-taro";

import prompt from '@/utils/prompt';
import { logout } from '@/service/api/common';
import { doGet, doPost, doPostJson, doPut, doDelete, doUpload, doDownload } from '../api';

export const Request: React.FC = () => {

  return <Space wrap>
    <Button type="primary" onClick={() => {
      doGet({ param1: "11", param2: "22" }).then((resp) => {
        console.log("GET请求->", resp);
        prompt.success("请求成功")
      })
    }}>GET</Button>

    <Button type="primary" onClick={() => {
      doPost({ param1: "11", param2: "22" }).then((resp) => {
        console.log("POST请求->", resp);
        prompt.success("请求成功")
      })
    }}>POST</Button>

    <Button type="primary" onClick={() => {
      doPostJson({ param1: "11", param2: "22" }).then((resp) => {
        console.log("postJson请求->", resp);
        prompt.success("请求成功")
      })
    }}>postJson</Button>

    <Button type="primary" onClick={() => {
      doPut({ param1: "11", param2: "22" }).then((resp) => {
        console.log("PUT请求->", resp);
        prompt.success("请求成功")
      })
    }}>PUT</Button>

    <Button type="primary" onClick={() => {
      doDelete({ param1: "11", param2: "22" }).then((resp) => {
        console.log("DELETE请求->", resp);
        prompt.success("请求成功")
      })
    }}>DELETE</Button>

    <Button type="primary" onClick={() => {
      Taro.chooseImage({
        success: (res) => {
          const tempFilePaths = res.tempFilePaths;
          const filePath = tempFilePaths[0];

          doUpload({ name: "file", filePath }, { param1: "1111" }).then((resp) => {
            console.log("UPLOAD请求->", resp);
            prompt.success("上传成功")
          })
        }
      })

    }}>UPLOAD</Button>

    <Button type="primary" onClick={() => {
      doDownload("download/setting.svg").then((resp) => {
        console.log("DOWNLOAD请求->", resp);
        prompt.success("下载成功")
      })
    }}>DOWNLOAD</Button>

    <Button type="primary" onClick={logout}>退出登录</Button>
  </Space>
}


