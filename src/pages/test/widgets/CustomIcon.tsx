import React from 'react';
import { Space } from "@nutui/nutui-react-taro";

import Icon from '@/components/icon';

export const CustomIcon: React.FC = () => {
  const size = 68
  return <Space wrap>
    <Icon name="home" color="red" size={size}/>
    <Icon name="my" color="blue"  size={size}/>
    <Icon name="setting" style={{fontSize: size, color: "green"}}/>
  </Space>
}


