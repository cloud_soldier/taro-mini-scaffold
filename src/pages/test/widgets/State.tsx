import React, { useContext, useState, useEffect } from 'react';
import { observer } from 'mobx-react';
import { View } from '@tarojs/components';
import { Space } from "@nutui/nutui-react-taro";

import { Ctx } from '../ctx';

export const State: React.FC = observer(() => {
  const { homeStore } = useContext(Ctx);
  const [count, setCount] = useState(0);

  useEffect(() => {
    let newCount = count;
    setInterval(() => {
      newCount++;
      setCount(newCount);
      homeStore.setCurrentTime(Date.now())
    }, 1000)
  }, [])

  return <Space wrap>
    <View>useState:{count}</View>
    <View>mobx:{homeStore.currentTime}</View>
  </Space>
})



