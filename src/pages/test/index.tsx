import React, { useMemo } from 'react';
import { observer } from 'mobx-react';
import { Collapse } from "@nutui/nutui-react-taro";
import { ArrowDown } from "@nutui/icons-react-taro"

import { Scaffold } from '@/layout';

import { HomeStore } from './store';
import { Ctx } from './ctx';
import { Navigation, Prompt, Request, State, CustomIcon, Tailwindcss } from "./widgets";

const components = [
  { label: "路由", Component: Navigation },
  { label: "提示和对话框", Component: Prompt },
  { label: "网路请求", Component: Request },
  { label: "状态管理", Component: State },
  { label: "自定义icon", Component: CustomIcon },
  { label: "tailwindcss样式", Component: Tailwindcss },
  // { label: "echart图表", Component: Chart },
];

const Test: React.FC = () => {
  const homeStore = useMemo(() => new HomeStore(), []);

  return <Scaffold
    loading={false}
    onShow={() => console.log("page->onSHow")}
    onHide={() => console.log("page->onHide")}
    onReady={() => console.log("page->onReady")}
    onUnload={() => console.log("page->onUnload")}
    // 下拉刷新
    onPullDownRefresh={() => new Promise<void>((resolve) => {
      setTimeout(() => {
        resolve();
      }, 2000)
    })}
  >
    <Ctx.Provider value={{ homeStore }}>

      <Collapse defaultActiveName={["0", "1", "2"]} expandIcon={<ArrowDown />}>
        {components.map(({ label, Component }, index) => <Collapse.Item
          key={index}
          title={label}
          name={index.toString()}
        >
          <Component />
        </Collapse.Item>)}
      </Collapse>

    </Ctx.Provider>
  </Scaffold>
}

export default observer(Test);

