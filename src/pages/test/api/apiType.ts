/**
 * 响应数据类型
 */
export declare namespace RespDataType {
  export interface CommonData{
    param1: string,
    param2: string,
  }

  export interface UploadData{
    param1: string,
    filename: string,
  }
}

/**
 * 请求数据类型
 */
export declare namespace RequestDataType {
  export interface CommonParams{
    param1: string,
    param2: string,
  }

  export interface UploadParams{
    name: string,
    filePath: string,
  }
}
