import { request,  } from "@/utils/T";

import { RespDataType, RequestDataType} from "./apiType";
export * from "./apiType";

const { get, post, postJSON, put, del, upload, download } = request;

/**
 * get请求
 */
export const doGet = (params: RequestDataType.CommonParams) => get<RespDataType.CommonData>("api/v1/system/testWeapp/get", params);

/**
 * post请求
 */
export const doPost = (params: RequestDataType.CommonParams) => post<RespDataType.CommonData>("api/v1/system/testWeapp/post", params);

/**
 * postJSON请求
 */
export const doPostJson = (params: RequestDataType.CommonParams) => postJSON<RespDataType.CommonData>("api/v1/system/testWeapp/postJson", params);

/**
 * put请求
 */
export const doPut = (params: RequestDataType.CommonParams) => put<RespDataType.CommonData>("api/v1/system/testWeapp/put", params);

/**
 * delete请求
 */
export const doDelete = (params: RequestDataType.CommonParams) => del<RespDataType.CommonData>("api/v1/system/testWeapp/delete", params);

/**
 * upload请求
 */
export const doUpload = (uploadParams: RequestDataType.UploadParams, otherParams?: {[key: string]: any}) => upload<RespDataType.UploadData>("api/v1/system/testWeapp/upload", uploadParams, otherParams);

/**
 * download请求
 */
export const doDownload = (sourceUrl: string) => download(sourceUrl);



