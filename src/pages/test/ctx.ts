import { createContext } from "react";
import type { HomeStore } from "./store";

// @ts-ignore
export const Ctx = createContext<{homeStore: HomeStore}>({})
