import React from 'react';
import { observer } from 'mobx-react';
import Taro from '@tarojs/taro';
import { Button } from "@nutui/nutui-react-taro";

import { Scaffold } from '@/layout';
import { EnumRouter } from '@/constants';

const Home: React.FC = () => {
  return <Scaffold>
    <Button type="primary" onClick={() => {
      Taro.navigateTo({url: EnumRouter.test.path})
    }}>跳转到测试页面</Button>
  </Scaffold>
}

export default observer(Home);

