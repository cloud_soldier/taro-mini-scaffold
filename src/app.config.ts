import { EnumTabBar, getMainPackagePages, getSubpackages } from "@/constants";
import { useGlobalIconFont } from '@/components/icon/iconfont/helper';

// 移除前缀斜线
const trimSuffix = (url: string) => url.replace(/^\//, "");

// 全局参数配置: https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/app.html
export default defineAppConfig({
  // 添加使用自定义iconFont组件
  usingComponents: Object.assign(useGlobalIconFont()),
  pages: getMainPackagePages(),
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },

  // 配置tab bar
  tabBar: {
    custom: EnumTabBar.custom,
    color: EnumTabBar.color,
    selectedColor: EnumTabBar.selectedColor,
    list: EnumTabBar.list.map(item => {
      const newItem = {...item};
      newItem.pagePath = trimSuffix(newItem.pagePath);
      if(newItem.iconPath) newItem.iconPath = trimSuffix(newItem.iconPath);
      if(newItem.selectedIconPath) newItem.selectedIconPath = trimSuffix(newItem.selectedIconPath);
      delete newItem.iconName;
      return newItem;
    })
  },

  // 配置分包
  subpackages: getSubpackages(),
})

