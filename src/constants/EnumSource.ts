import { EnumEnv } from "./EnumEnv";
/**
 * 转换完整的远程资源地址
 */
const toS = (url: string) => {
  if (/^http/.test(url)) return url;
  return EnumEnv.sourceDomain.replace(/\/$/, "") + "/" + url.replace(/^\//, "");
}

/**
 * 枚举资源
 */
export const EnumSource = {
  // 本地资源
  local:{
    loginBgImg: "/assets/images/loginBg.jpg", // 登录页背景图片
    logoImg: "/assets/images/logo.png", // 图片logo
    homeImg: "/assets/images/home.png",
    homeSelectedImg: "/assets/images/select-home.png",
    myImg: "/assets/images/my.png",
    mySelectedImg: "/assets/images/select-my.png",
  },

  // 远程资源
  remote:{
    testImg: toS("xxx.png"),
  }
}
