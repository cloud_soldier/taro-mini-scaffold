/**
 * 枚举环境变量
 */
export const EnumEnv = {
  apiDomain: "http://170.16.0.193:6041",        // api地址
  sourceDomain: "http://170.16.0.193:6041",     // 远程资源地址
  apiResponseCode: {
    // apiSuccessCode: "200",      // 请求成功响应code
    apiSuccessCode: "0",           // 请求成功响应code
    errorCode: "error",            // 请求失败响应code
    noLoginCode: "401",            // 未登录的code
    noPermissionCode: "403",       // 没有权限的code
    invalidParamCode: "400",       // 参数校验失败code
  },
  app:{
    title: "视视通",  // app名称
  }
};
