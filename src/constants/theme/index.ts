
/**
 * 配置nutui主题
 */
export const EnumNutuiTheme = {
  nutuiColorPrimary: '#468FFC',
  nutuiColorPrimaryStop1: '#468FFC',
  nutuiColorPrimaryStop2: '#468FFC',
}

/**
 * 配置应用主题
 */
export const EnumAppTheme = {
  pageBgColor: "#F6F7F8", // 页面背景色
}
