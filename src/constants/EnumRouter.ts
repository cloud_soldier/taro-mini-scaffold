// import Taro from "@tarojs/taro";
import type { IconNames } from "@/components/icon";
// import { request } from "@/utils/T";

const subPackageBaseDir = "subpackages";              // 分包的根目录名称
const getFirstPagePath = () => EnumRouter.home.path;  // 入口页面路径

/**
 * 枚举路由地址
 * pageConfig配置参数说明: https://developers.weixin.qq.com/miniprogram/dev/reference/configuration/page.html
 */
export const EnumRouter = {
  // **************************配置主包中相关的路由**************************
  home: {
    path: "/pages/home/index",
    pageConfig: {
      navigationBarTitleText: "首页",
      enablePullDownRefresh: true,  // 开启下来刷新
    },
  },
  my: {
    path: "/pages/my/index",
    pageConfig: {
      navigationBarTitleText: "我的",
    },
  },
  login: {
    path: "/pages/login/index",
    pageConfig: {
      navigationBarTitleText: "登录",
    },
  },
  webview: {
    path: "/pages/webview/index",
    pageConfig: {
      navigationBarTitleText: "webview",
    },
  },
  test: {
    path: "/pages/test/index",
    pageConfig: {
      navigationBarTitleText: "测试",
    },
  },

  // **************************配置分包中相关的路由, 注意： subpackages一定要是subPackageBaseDir变量中的值**************************
  subpackages: {
    packageA: {
      test: {
        path: "/subpackages/packageA/pages/test/index",
        pageConfig: {
          navigationBarTitleText: "packageA-test",
        },
      }
    },
    packageB: {
      test: {
        path: "/subpackages/packageB/pages/test/index",
        pageConfig: {
          navigationBarTitleText: "packageB-test",
        },
      }
    }
  }
};

/**
 * 配置底部tab bar
 */
export const EnumTabBar: TabBar = {
  custom: true,
  height: 50,
  color: '#ACADAE',
  selectedColor: '#468FFC',
  list: [
    {
      text: EnumRouter.home.pageConfig.navigationBarTitleText,
      pagePath: EnumRouter.home.path,
      iconName: "home",
    },
    {
      text: EnumRouter.my.pageConfig.navigationBarTitleText,
      pagePath: EnumRouter.my.path,
      iconName: "my",
    },
  ]
};


interface TabBar {
  custom: boolean,   // 使用自定义的tab-bar: src/custom-tab-bar
  height: number,     // tabbar高度
  color: string,
  selectedColor: string,
  list: {
    text: string,
    pagePath: string,
    iconName?: IconNames,
    iconPath?: string,
    selectedIconPath?: string,
  }[]
}


const trimSuffix = (url: string) => url.replace(/^\//, "");

function formatUrlParams(url: string, params: {[k:string]: any} = {}) {
  Object.keys(params).forEach((key, index) => {
    if (index === 0 && url.indexOf('?') === -1) {
      url += '?' + key + '=' + params[key];
    } else {
      url += '&' + key + '=' + params[key];
    }
  });

  return url;
}

/**
 * webview页面path
 */
export const getWebviewPagePath = (url: string, urlParams: {[k:string]: any} = {}) => {
  const redirectUrl = formatUrlParams(url, urlParams);
  return `${EnumRouter.webview.path}?redirectUrl=${encodeURIComponent(redirectUrl)}`;

  // Taro.navigateTo({
  //   // url: `${EnumRouter.webview.path}?redirectUrl=${encodeURIComponent("https://www.baidu.com?name=aaa")}`,
  //   url: `${EnumRouter.webview.path}?redirectUrl=${encodeURIComponent(redirectUrl)}`,
  // })
}

/**
 * 获取主包页面列表
 */
export const getMainPackagePages = () => {
  const pages: string[] = [trimSuffix(getFirstPagePath())];

  Object.keys(EnumRouter).forEach(key => {
    if (key != subPackageBaseDir) {
      const pagePath = trimSuffix(EnumRouter[key].path);
      if (!pages.includes(pagePath)) {
        pages.push(pagePath);
      }
    }
  });

  return pages;
}


interface SubPackageConf {
  root: string,
  pages: string[],
}

/**
 * 获取分包配置
 */
export const getSubpackages = () => {
  let subpackages: SubPackageConf[] = [];

  // @ts-ignore
  const enumSubPackages = EnumRouter.subpackages || {};
  if (enumSubPackages) {
    subpackages = Object.keys(enumSubPackages).map(packageName => {
      const p = enumSubPackages[packageName];
      const root = `${subPackageBaseDir}/${packageName}`;
      const packageConf: SubPackageConf = {
        root,
        pages: Object.values(p).map((item: any) => item.path.replace("/" + root + "/", ""))
      }

      return packageConf
    })
  }

  return subpackages;
}
