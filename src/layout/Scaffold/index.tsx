import React, { useMemo, PropsWithChildren } from 'react';
import Taro, { useDidHide, useDidShow, useLoad, useReady, useUnload, usePullDownRefresh, } from '@tarojs/taro'
import { View } from '@tarojs/components';
import { ConfigProvider, SafeArea, Loading, Dialog, Toast } from "@nutui/nutui-react-taro";
import zhCN from "@nutui/nutui-react-taro/dist/locales/zh-CN";

import { EnumNutuiTheme, EnumAppTheme, EnumTabBar } from '@/constants';
import prompt from '@/utils/prompt';

export interface ScaffoldProps {
  className?: string,
  style?: React.CSSProperties,
  loading?: boolean,
  onLoad?: (options?: any) => void,
  onShow?: (options?: any) => void,
  onReady?: () => void,
  onHide?: () => void,
  onUnload?: () => void,
  onPullDownRefresh?: () => Promise<any>,  // 下拉刷新回调函数
}

export const Scaffold: React.FC<PropsWithChildren<ScaffoldProps>> = ({ children, ...props }) => {
  const { className, style, loading } = props;
  const { isTabBarPage } = useScaffold(props);

  const isOffsetTabBar = EnumTabBar.custom && isTabBarPage; // 是否抵消自定义tabbar遮挡的部分
  const content = loading ? <View className='h-full flex items-center justify-center'>
    <Loading type="spinner" />
  </View> : children

  return <ConfigProvider
    className={` h-full box-border ${isOffsetTabBar ? "flex flex-col" : ""} ${className}`}
    theme={EnumNutuiTheme}
    locale={zhCN}
    style={{
      backgroundColor: EnumAppTheme.pageBgColor,
      ...style
    }}
  >
    {isOffsetTabBar ? <View className="grow h-0 overflow-auto">{content}</View> : content}

    {/* 如果是属于tabbar页面，则填充高度，这样可以抵消自定义tabbar遮挡的部分 */}
    {isOffsetTabBar && <View style={{ height: EnumTabBar.height }} />}

    {/* Toast占位, 配合prompt使用 */}
    <Toast id={prompt.toastId} />

    {/* Dialog占位, 配合prompt使用 */}
    <Dialog id={prompt.dialogId} />
    {/* 在全面屏下提供自适应的边距调整 */}
    <SafeArea position="bottom" />
  </ConfigProvider>
}

Scaffold.defaultProps = {
  loading: false,
  className: "",
}

// 微信小程序页面声明周期参照: https://developers.weixin.qq.com/miniprogram/dev/reference/api/Page.html
const useScaffold = ({ onLoad, onShow, onReady, onHide, onUnload, onPullDownRefresh }: ScaffoldProps) => {
  const isTabBarPage = useMemo(() => {
    const pages = Taro.getCurrentPages();
    const currentPage = pages[pages.length - 1];
    return EnumTabBar.list.findIndex(p => p.pagePath.includes(currentPage.route ?? "null")) >= 0
  }, []);

  // 对应 onLoad
  // 页面加载时触发。一个页面只会调用一次，可以在 onLoad 的参数中获取打开当前页面路径中的参数
  if (onLoad) useLoad(onLoad);

  // 对应 onReady
  // 页面初次渲染完成时触发。一个页面只会调用一次，代表页面已经准备妥当，可以和视图层进行交互
  // 注意：对界面内容进行设置的 API 如wx.setNavigationBarTitle，请在onReady之后进行
  if (onReady) useReady(onReady);

  // 对应 onShow
  // 页面显示/切入前台时触发
  if (onShow) useDidShow(onShow);

  // 对应 onHide
  // 页面隐藏/切入后台时触发。 如 wx.navigateTo 或底部 tab 切换到其他页面，小程序切入后台等。
  if (onHide) useDidHide(onHide);

  // 对应 onUnload
  // 页面卸载时触发。如wx.redirectTo或wx.navigateBack到其他页面时。
  if (onUnload) useUnload(onUnload);

  // 对应 onPullDownRefresh
  // 监听用户下拉刷新事件
  if (onPullDownRefresh) {
    usePullDownRefresh(() => {
      // 开启加载动画
      prompt.loading();

      const fn = () => {
        wx.stopPullDownRefresh()
        prompt.hide();
      }

      onPullDownRefresh().then(fn).catch(fn)
    })
  }

  return { isTabBarPage }
}

