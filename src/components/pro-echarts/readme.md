# pro-echarts

## assets/echarts.min.js文件生成的配置
```
# 1. 定制echarts官网: https://echarts.apache.org/zh/builder.html

# 2. 以下是定制echarts的选项，如果有修改请在此记录
  (1). 图表选择: 折线图
  (2). 坐标系: 直角坐标系
  (3). 组件: 标题, 图例, 提示框
  (4). 其他选项: 工具集, 代码压缩
```
