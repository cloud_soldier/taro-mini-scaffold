import React, { useEffect, useRef } from 'react';
// 官方文档: https://github.com/qiuweikangdev/taro-react-echarts
import Echarts, { EChartOption, EchartsHandle } from 'taro-react-echarts';
// @ts-ignore
import echarts from './assets/echarts.min.js'

export type { EChartOption }


interface BarProps {
  width?: number,
  height?: number,
  style?: React.CSSProperties,
  option: EChartOption
  getEchartsHandle?: (e: EchartsHandle) => void
}

const ProEchart: React.FC<BarProps> = ({ width, height, style, option, getEchartsHandle }) => {
  const echartsRef = useRef<EchartsHandle>(null)

  useEffect(() => {
    getEchartsHandle && getEchartsHandle(echartsRef.current as EchartsHandle)
  }, [])

  return <Echarts
    style={{ width: `${width}%`, height: `${height}%`, ...style }}
    type='canvas'
    echarts={echarts} // 支持自定义构建
    option={option}
    ref={echartsRef}
    isPage={false}
  />
};

ProEchart.defaultProps = {
  width: 100,
  height: 100,
};

export default ProEchart;
