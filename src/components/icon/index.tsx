import React from "react";
import IconFont, { type IconNames } from "./iconfont/index.weapp";

export type { IconNames }

export interface IconProps {
  name: IconNames;
  /** 图标大小 */
  size?: number;
  /** 图标颜色 当原SVG图为单色时可修改颜色 */
  color?: string | string[];
  style?: React.CSSProperties;
}

const Icon: React.FC<IconProps> = ({ style, color, size, ...props }) => {
  return <IconFont
    {...props}
    color={color ?? style?.color}
    size={size ?? (style?.fontSize ? parseFloat(style?.fontSize.toString()) : undefined)}
    style={style}
  />
};

export default Icon;
