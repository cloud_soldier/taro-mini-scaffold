import React from 'react';
import Taro from '@tarojs/taro';
import { CoverView } from '@tarojs/components';

import Icon from "@/components/icon"
import { EnumTabBar } from "@/constants";
import './index.scss'

const { height, color, selectedColor, list } = EnumTabBar;

const TabBarNative: React.FC = () => {
  const currentPath = Taro.getCurrentInstance().router?.path ?? list[0].pagePath;

  return <CoverView className='tab-bar' style={{height}}>
    <CoverView className='tab-bar-border'></CoverView>
    {list.map((item) => {
      const pagePath = item.pagePath;
      const selected = isEqualPath(currentPath, item.pagePath)
      const currentColor = selected ? selectedColor: color;

      return (
        <CoverView key={pagePath} className='tab-bar-item' onClick={() => Taro.switchTab({url: pagePath})}>
          <Icon
            // @ts-ignore
            name={item.iconName}
            color={currentColor}
          />
          <CoverView style={{ color: currentColor }}>{item.text}</CoverView>
        </CoverView>
      )
    })}
  </CoverView>
}

export default TabBarNative;

const isEqualPath = (a: string, b: string) => (a || '').replace(/^\//, '') === (b || '').replace(/^\//, '')
