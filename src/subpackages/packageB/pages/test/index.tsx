import React from 'react';
import { View } from '@tarojs/components';

import { Scaffold } from '@/layout';
import { Chart } from './Chart';

const Test: React.FC = () => {

  return <Scaffold>
    <View>我是packageB分包中的test页面</View>
    <Chart />
  </Scaffold>
}

export default Test
