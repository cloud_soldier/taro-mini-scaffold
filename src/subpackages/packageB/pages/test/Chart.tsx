import React from 'react';

import ProEchart from '@/components/pro-echarts'

interface ChartProps{

}

export const Chart: React.FC<ChartProps> = () => {
  const option = {
    tooltip: {
      trigger: 'axis',
      show: true,
      confine: true
    },
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [150, 230, 224, 218, 135, 147, 260],
        type: 'line'
      }
    ]
  }

  return <ProEchart style={{ height: 260 }} option={option} />
}

