import React from 'react';
import Taro from '@tarojs/taro';
import { View } from '@tarojs/components';
import { Button } from '@nutui/nutui-react-taro';

import { Scaffold } from '@/layout';
import { EnumRouter } from '@/constants';
import { Chart } from './Chart';

const Test: React.FC = () => {

  return <Scaffold>
    <View>我是packageA分包中的test页面</View>
    <Button type="primary" onClick={() =>{
      Taro.switchTab({url: EnumRouter.my.path})
    }}>跳转到主包我的</Button>
    <Chart />
  </Scaffold>
}

export default Test
