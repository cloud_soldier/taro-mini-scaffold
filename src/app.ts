import { useEffect } from 'react';
import { redirectTo,useLaunch, useDidShow, useDidHide, useError, usePageNotFound, useUnhandledRejection } from '@tarojs/taro';

import { request } from '@/utils/T';
import { EnumEnv, EnumRouter } from '@/constants';

// 全局样式
import './app.scss';

// 生命周期参照文档: https://developers.weixin.qq.com/miniprogram/dev/reference/api/App.html
function App(props) {
  // 可以使用所有的 React Hooks
  useEffect(() => {
    console.log("App -> useEffect")
    // 设置api请求公共配置
    request.setRequestConf({
      apiDomain: EnumEnv.apiDomain,
      apiResponseCode:EnumEnv.apiResponseCode,
      redirectLogin: () => redirectTo({url: EnumRouter.login.path}),  // 重定向到登录页
    });
  })

  // 对应 onLaunch
  useLaunch(() => {
    console.log("App -> useLaunch")
  })

  // 对应 onShow
  useDidShow(() => {
    console.log("App -> useDidShow")
  })

  // 对应 onHide
  useDidHide(() => {
    console.log("App -> useDidHide")
  })

  // 对应 onError
  useError((errorMsg) => {
    console.error("App -> useError->errorMsg->", errorMsg)
  })

  // 对应 onPageNotFound
  usePageNotFound((params) => {
    console.error("App -> usePageNotFound -> params->", params)
  })

  // 对应 onUnhandledRejection
  useUnhandledRejection((error) => {
    console.error("App -> useUnhandledRejection ->", error.reason)
  })

  return props.children
}

export default App
