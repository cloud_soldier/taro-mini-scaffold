/**
 * 响应数据类型
 */
export declare namespace RespCommonDataType {
  interface LabelValueOfColor<T> extends LabelValue<T> {
    color: string
  }

  // 全局配置
  export interface IGlobalConfig {
    /*******公共状态相关的枚举********/
    commonStatus: {
      normal: number,   // 正常
      stop: number,     // 停止
    },
    commonStatusList: LabelValueOfColor<number>[],

    /*******用户类型相关的枚举********/
    userType: {
      fb: number,     // 前后台用户
      front: number,  // 前台用户
    },
    userTypeList: LabelValueList<number>,

    /*******系统角色类型相关的枚举********/
    adminRoleType: {
      superAdmin: number, // 超级管理员
      admin: number,      // 管理员
      normal: number,     // 普通用户
      operator: number,   // 实施人员
    },
    adminRoleTypeList: LabelValueList<number>,

    /*******角色类型允许用户使用相关的枚举********/
    roleAllowUserUseType: {
      allow: number,      // 允许
      notAllow: number,   // 不允许
    },
    roleAllowUserUseTypeList: LabelValueList<number>,

    /*******角色类型允许用户使用相关的枚举********/
    roleDataScopeType: {
      all: number,           // 全部数据权限
      custom: number,        // 自定数据权限
      selfDept: number,      // 本部门数据权限
      selfDeptAll: number,   // 本部门及以下数据权限
      my: number,            // 仅本人数据权限
    },
    roleDataScopeTypeList: LabelValueList<number>,
  }
}

/**
 * 请求数据类型
 */
export declare namespace RequestCommonDataType {

}
