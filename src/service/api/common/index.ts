import { reLaunch } from "@tarojs/taro";

import prompt from "@/utils/prompt";
import {request} from "@/utils/T";
import { EnumRouter } from "@/constants";

import { RespCommonDataType } from "./apiType";

export * from "./apiType";

const { get, removeToken } = request;

/**
 * 获取全局配置
 */
export const getGlobalConfig = () => get<RespCommonDataType.IGlobalConfig>("api/v1/globalConfig").then(resp => {
  // 1. 为公共状态列表添加颜色
  const { commonStatusList, commonStatus } = resp.data;
  resp.data.commonStatusList = commonStatusList.map(item => {
    switch(item.value){
      case commonStatus.normal:
        item.color = "green";
        return item
      case commonStatus.stop:
        item.color = "red"
        return item;
      default:
        return item
    }
  });

  return resp;
});


/**
 * 退出登录
 */
export const logout = () => get("api/v1/system/logout", {}, {isShowErrorPrompt: false}).then(() => {
  removeToken();
  reLaunch({url: EnumRouter.login.path});
}).catch((e) => {
  prompt.error("退出登录失败");
  console.error("退出登录报错:", e);
})

