/// <reference types="@tarojs/taro" />

// 引入微信小程序的wx全局变量相关的类型系统
/// <reference path="../node_modules/miniprogram-api-typings/index.d.ts" />

declare module '*.png';
declare module '*.gif';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';
declare module '*.css';
declare module '*.less';
declare module '*.scss';
declare module '*.sass';
declare module '*.styl';

declare namespace NodeJS {
  interface ProcessEnv {
    TARO_ENV: 'weapp' | 'swan' | 'alipay' | 'h5' | 'rn' | 'tt' | 'quickapp' | 'qq' | 'jd'
  }
}

// 分页类型
interface PageList<T> {
  total: number;
  list: T[]
}

interface LabelValue<T = any> {
label: string,
value: T
}

// label value列表类型
type LabelValueList<T = any> = LabelValue<T>[]

// loading data
interface LoadingData<T>{
loading: boolean,
data: T
}

